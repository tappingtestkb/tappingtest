﻿using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using Microsoft.Office.Interop.Excel;

namespace TappingTest
{ 
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window, IRepo
    {
        List<PersonInfo> pList;
        DispatcherTimer timer = new DispatcherTimer();
        PersonInfo newPerson = new PersonInfo();
        bool timerFlag = true;
        bool graphSecondFlag = false;
        bool saveFlag = true;
        private bool OpenExcel = false;
        int[] clickCount = new int[6]; 
        Microsoft.Office.Interop.Excel.Application ex;
        Workbook wb;

        public MainWindow()
        {
            InitializeComponent();
            pList = new List<PersonInfo>();
            nameColumn.Binding = new Binding("name");
            dateColumn.Binding = new Binding("date");
        }
        public void Window_Closed(object sender, EventArgs e)
        {
            if (OpenExcel)
            {
                try
                {
                    wb.Close(false, Type.Missing, Type.Missing);
                    ex.Quit();
                }
                catch(Exception)
                {
                    try
                    {
                        ex.Quit();
                    }
                    catch (Exception) { }
                }
            }
        }

        #region TestingWindow
        #region Timer
        private void timerStart()
        {
            timerFlag = false;
            timer.Tick += new EventHandler(timerTick);
            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Start();
        }
        private void timerTick(object sender, EventArgs e)
        {
            int sec = int.Parse(labelTimer.Content.ToString());
            labelTimer.Content = sec <= 10 ? $"0{--sec}" : $"{--sec}";
            if (labelTimer.Content.ToString() == "00")
            {
                timer.Stop();
                labelBeginButton.Text = "Время вышло";
                var plot = new Graphic(clickCount);
                plotViewTest.Model = plot.Model();
            }
        }
        #endregion
        public void test_Click(object sender, RoutedEventArgs e)
        {
            mainMenu.Visibility = Visibility.Collapsed;
            testMenu.Visibility = Visibility.Visible;
            toStandartSettings();
        }
        public void save_Click(object sender, RoutedEventArgs e)
        {
            if (saveFlag)
            {
                if ((boxName.Text.Length != 0) & (labelTimer.Content.ToString() == "00"))
                {
                    newPerson.point = clickCount;
                    newPerson.name = boxName.Text.ToString();
                    newPerson.date = DateTime.Now.ToShortDateString();
                    newPerson.type = "Тип не определён";
                    if (Write(newPerson))
                    {
                        MessageBox.Show("Данные успешно сохранены", "Сохранено", MessageBoxButton.OK, MessageBoxImage.Information);
                        boxName.IsEnabled = false;
                        saveFlag = false;
                    }
                }
                else if (boxName.Text.Length == 0)
                {
                    MessageBox.Show("Введите ФИ", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                {
                    MessageBox.Show("Пройдите тест", "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public void back_Click(object sender, RoutedEventArgs e)
        {
            testMenu.Visibility = Visibility.Collapsed;
            mainMenu.Visibility = Visibility.Visible;
        }
        private void toStandartSettings()
        {
            timer.Stop();
            timer.Tick -= new EventHandler(timerTick);
            timerFlag = true;
            labelBeginButton.Text = "Начать";
            labelTimer.Content = "30";
            saveFlag = true;
            clickCount = new int[6];
            boxName.IsEnabled = true;
            boxName.Text = "";
            Graphic plot = new Graphic();
            plotViewTest.Model = plot.Model();
        }
        public void start_Click(object sender, RoutedEventArgs e)
        {
            if (timerFlag)
            {
                startButton.Background = new SolidColorBrush(Color.FromRgb(255, 255, 0));
                labelBeginButton.Text = "Нажимайте";
                timerStart();
            }
            else
            {
                if (timer.IsEnabled)
                {
                    clickCount[5 - ((int.Parse(labelTimer.Content.ToString()) - 1) / 5)]++;
                }
            }
        }
        #endregion        
        public void res_Click(object sender, RoutedEventArgs e)
        {
            mainMenu.Visibility = Visibility.Hidden;
            infoMenu.Visibility = Visibility.Visible;
            Read();
            pList = new List<PersonInfo>();
            pList.AddRange(resList.Items.OfType<PersonInfo>());
            Graphic plot = new Graphic();
            plotViewInfo1.Model = plot.Model();
            plotViewInfo2.Model = plot.Model();
        }
        public void reset_Click(object sender, RoutedEventArgs e)
        {
            resList.Items.Clear();
            foreach(PersonInfo p in pList)
            {
                resList.Items.Add(p);
            }
            if (datePick.SelectedDate.HasValue)
            {
                datePick.SelectedDate = null;
            }
            searchFio.Text = "";
            Graphic plot = new Graphic();
            plotViewInfo1.Model = plot.Model();
            plotViewInfo2.Model = plot.Model();
        }
        public void search_Click(object sender, RoutedEventArgs e)
        {
            Predicate<PersonInfo> predicate = Find;
            List<PersonInfo> found = new List<PersonInfo>();
            found.AddRange(pList);
            found = found.FindAll(predicate);
            resList.Items.Clear();
            foreach (PersonInfo p in found)
            {
                resList.Items.Add(p);
            }
        }
        public bool Find(PersonInfo p)
        {
            string fio = "*";
            if (searchFio.Text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Length != 0)
            {
                fio = searchFio.Text;
            }
            if (fio != "*")
            {
                if (fio != p.name)
                {
                    return false;
                }
            }
            if (datePick.SelectedDate.HasValue)
            {
                DateTime d = (DateTime)datePick.SelectedDate;
                if (d.ToShortDateString() != p.date)
                {
                    return false;
                }
            }
            return true;
        }
        public void backFromInf_Click(object sender, RoutedEventArgs e)
        {
            mainMenu.Visibility = Visibility.Visible;
            infoMenu.Visibility = Visibility.Hidden;
        }
        public async void Read()
        {
            resList.Items.Clear();
            await Task.Run(() => OpenExcelFileAndRead());
        }
        private void OpenExcelFileAndRead()
        {
            List<PersonInfo> list = new List<PersonInfo>();
            DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory);
            di = di.Parent;
            di = di.Parent;
            di = di.Parent;
            di = di.Parent;
            ex = new Microsoft.Office.Interop.Excel.Application();
            try
            {
                wb = ex.Workbooks.Open(di.FullName + "\\результаты tappingtest");
                
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                object misValue = System.Reflection.Missing.Value;
                wb = ex.Workbooks.Add(misValue);
                wb.SaveAs(di.FullName + "\\результаты tappingtest");
                MessageBox.Show("Не удалось найти Excel файл с результатами, поэтому был создан новый Excel файл результаты tappingtest.xlsx", 
                    "Предупреждение", MessageBoxButton.OK, MessageBoxImage.Warning);
                ex.Quit();
                Dispatcher.Invoke(() => backFromInf_Click(this, null));
                return;
            }
            catch (Exception)
            {
                MessageBox.Show("Непредвиденная ошибка при попытке окрыть Excel файл.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                ex.Quit();
                Dispatcher.Invoke(() => backFromInf_Click(this, null));
                return;
            }
            OpenExcel = true;
            Worksheet ws = wb.Sheets[1];
            Range lastCell = ws.Cells.SpecialCells(XlCellType.xlCellTypeLastCell);
            int lastRow = lastCell.Row;
            for (int j = 1; j < lastRow; j++)
            {
                if (ws.Cells[j + 1, 1].Text.ToString() == string.Empty)
                {
                    break;
                }
                PersonInfo p = null;
                try
                {
                    p = new PersonInfo(ws.Cells[j + 1, 1].Text.ToString(), ws.Cells[j + 1, 3].Text.ToString(),
                        new DateTime(Convert.ToInt32(ws.Cells[j + 1, 2].Text.ToString().Split('.')[2]), Convert.ToInt32(ws.Cells[j + 1, 2].Text.ToString().Split('.')[1]),
                        Convert.ToInt32(ws.Cells[j + 1, 2].Text.ToString().Split('.')[0])), new int[] { Convert.ToInt32(ws.Cells[j + 1, 4].Text.ToString()),
                        Convert.ToInt32(ws.Cells[j + 1, 5].Text.ToString()), Convert.ToInt32(ws.Cells[j + 1, 6].Text.ToString()),
                        Convert.ToInt32(ws.Cells[j + 1, 7].Text.ToString()), Convert.ToInt32(ws.Cells[j + 1, 8].Text.ToString()), Convert.ToInt32(ws.Cells[j + 1, 9].Text.ToString()) });
                }
                catch(Exception)
                {
                    MessageBox.Show("Ошибка чтения строки №" + (j + 1) + ", проверьте её на правильность данных.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                    continue;
                }
                Dispatcher.Invoke(() => pList.Add(p));
                Dispatcher.Invoke(() => resList.Items.Add(p));
            }
            wb.Close(false, Type.Missing, Type.Missing);
            ex.Quit();
            OpenExcel = false;
        }
        public bool Write(PersonInfo p)
        {
            DirectoryInfo di = new DirectoryInfo(Environment.CurrentDirectory);
            di = di.Parent;
            di = di.Parent;
            di = di.Parent;
            di = di.Parent;
            ex = new Microsoft.Office.Interop.Excel.Application();
            try
            {
                wb = ex.Workbooks.Open(di.FullName + "\\результаты tappingtest");
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                object misValue = System.Reflection.Missing.Value;
                wb = ex.Workbooks.Add(misValue);
                wb.SaveAs(di.FullName + "\\результаты tappingtest");

            }
            catch (Exception)
            {
                MessageBox.Show("Непредвиденная ошибка при попытке окрыть Excel файл.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                ex.Quit();
                return false;
            }
            OpenExcel = true;
            Worksheet ws = wb.Sheets[1];
            Range lastCell = ws.Cells.SpecialCells(XlCellType.xlCellTypeLastCell);
            int lastRow = lastCell.Row;
            int i;
            for (i = 1; i < lastRow; i++)
            {
                if (ws.Cells[i + 1, 1].Text.ToString() == string.Empty)
                {
                    break;
                }
            }
            ws.Cells[i + 1, 1] = p.name;
            ws.Cells[i + 1, 2] = p.date;
            ws.Cells[i + 1, 3] = p.type;
            ws.Cells[i + 1, 4] = p.point[0];
            ws.Cells[i + 1, 5] = p.point[1];
            ws.Cells[i + 1, 6] = p.point[2];
            ws.Cells[i + 1, 7] = p.point[3];
            ws.Cells[i + 1, 8] = p.point[4];
            ws.Cells[i + 1, 9] = p.point[5];
            ex.Application.ActiveWorkbook.Save();
            wb.Close(false, Type.Missing, Type.Missing);
            ex.Quit();
            OpenExcel = false;
            return true;
        }
        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (searchFio.Text.Length != 0 && searchFio.Text[searchFio.Text.Length - 1] == '*')
            {
                MessageBox.Show("Введен запрещенный символ *.", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                searchFio.Text = searchFio.Text.TrimEnd(new char[] { '*' });
            }
        }
        private void resList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PersonInfo a = (PersonInfo)resList.SelectedItem;
            if (graphSecondFlag)
            {
                if (a != null)
                {
                    var plot = new Graphic(a.point, a.name);
                    plotViewInfo2.Model = plot.Model();
                    graphSecondFlag = false;
                }
            }
            else if (a != null)
            {
                var plot = new Graphic(a.point, a.name);
                plot.Title = a.name;
                plotViewInfo1.Model = plot.Model();
                graphSecondFlag = true;
            }
        }
    }
    public interface IRepo
    {
        void Read();
        bool Write(PersonInfo p);
    }
    public class PersonInfo
    {
        public string name { get; set; }
        public string type { get; set; }
        public string date { get; set; }
        public int[] point;
        public PersonInfo(string n, string t, DateTime d, int[] p)
        {
            point = p;
            name = n;
            type = t;
            date = d.ToShortDateString();
        }

        public PersonInfo()
        {
        }
    }
    public class Graphic
    {
        public int[] clickCount { get; set; }
        public string Title { get; set; }
        public Graphic() { clickCount = new int[] {}; }
        public Graphic(int[] clickCount) { this.clickCount = clickCount; }
        public Graphic(int[] clickCount, string title)
        {
            this.clickCount = clickCount;
            this.Title = title;
        }
        public PlotModel Model()
        {
            var plotModel = new PlotModel
            {
                PlotAreaBorderThickness = new OxyThickness(1, 0, 0, 1),
                PlotMargins = new OxyThickness(20)
            };
            var firstLinearAxis = new LinearAxis
            {
                AbsoluteMaximum = clickCount.Length != 0 ? clickCount.Max() + 10 : 50,
                AbsoluteMinimum = 0,
                PositionAtZeroCrossing = true,
                IntervalLength = 25,
                Title = "Клики",
                TitleColor = OxyColor.FromRgb(255,255,255),
                TickStyle = TickStyle.Crossing
            };
            plotModel.Axes.Add(firstLinearAxis);
            var secondLinearAxis = new LinearAxis
            {
                AbsoluteMaximum = 35,
                AbsoluteMinimum = 0,
                PositionAtZeroCrossing = true,
                Position = AxisPosition.Bottom,
                IntervalLength = 45,
                Title = "Время",
                TitleColor = OxyColor.FromRgb(255,255,255),
                TickStyle = TickStyle.Crossing
            };
            plotModel.Axes.Add(secondLinearAxis);
            if (clickCount.Length != 0) 
            {
                FunctionSeries fs = new FunctionSeries();
                fs.Points.Add(new DataPoint(5, clickCount[0]));
                fs.Points.Add(new DataPoint(10, clickCount[1]));
                fs.Points.Add(new DataPoint(15, clickCount[2]));
                fs.Points.Add(new DataPoint(20, clickCount[3]));
                fs.Points.Add(new DataPoint(25, clickCount[4]));
                fs.Points.Add(new DataPoint(30, clickCount[5]));
                fs.Color = OxyColor.FromRgb(0, 0, 0);
                fs.CanTrackerInterpolatePoints = false;
                plotModel.Series.Add(fs);
            }
            plotModel.Title = this.Title;
            return plotModel;
        }
    }
}
